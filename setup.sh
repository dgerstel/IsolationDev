#!/bin/bash
# - Setup a local DV dev folder
# - Copy there isolation Tuple Tools and compile them
PLATFORM=x86_64-slc6-gcc49-opt
DVVER=v42r3
# Pick analysis version that corresponds to the DV version
ANALYSISVER="$(lb-run DaVinci/$DVVER echo \$ANALYSIS_PROJECT_ROOT | sed -r 's/.*ANALYSIS_(v.*)/\1/')"
DVFOLDER=$HOME # Where you wish your DV dev to live
mkdir -p $DVFOLDER
DVDEV=${DVFOLDER}/DaVinciDev_${DVVER}
ISODEVDIR=$PWD
LbLogin -c $PLATFORM && \
cd $DVFOLDER
# Either a DV dev folder exists or make one
[[ -e `basename ${DVDEV}` || $(lb-dev DaVinci $DVVER) ]] && \
cd DaVinciDev_${DVVER}/ && \
git lb-use Analysis && \
git lb-checkout Analysis/$ANALYSISVER Phys/DecayTreeTuple && \
cp ${ISODEVDIR}/src/* ${DVDEV}/Phys/DecayTreeTuple/src/ && \
lb-run DaVinci/${DVVER} make -j8 -C $DVDEV configure && \
lb-run DaVinci/${DVVER} make -j8 -C $DVDEV install

