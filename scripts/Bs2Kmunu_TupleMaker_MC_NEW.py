### __author__ : Basem Khanji ,  basem.khanji@cern.ch 
#######################################################################
from Gaudi.Configuration import *
#######################################################################
#
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence , MergedSelection
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco , LoKi__Hybrid__EvtTupleTool, MCTupleToolInteractions , L0TriggerTisTos , TriggerTisTos , TupleToolIsoGeneric , MCTupleToolHierarchyNTracks

from Configurables import FilterDesktop , CombineParticles
from Configurables import TupleToolEventInfo 
################################################################################################################################                        
# Re-Run the Stripping :
# Define here the pion cuts :
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import EventNodeKiller
stripping             = 'stripping26' # 
config                = strippingConfiguration(stripping)
archive               = strippingArchive(stripping)
streams               = buildStreams(stripping=config, archive=archive)
eventNodeKiller       = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
DaVinci().appendToMainSequence( [ eventNodeKiller ] )
MyStream = StrippingStream("MyStream")
#MyLines = [ 'StrippingB2XuMuNuBs2KLine']

MyLines = [ 'StrippingB2XuMuNuBs2K_FakeKMuLine' , 'StrippingB2XuMuNuBs2KLine' , 'StrippingB2XuMuNuBs2KSSLine' ]
Lines_to_run = []
for stream in streams:
    for line in stream.lines:
        if line.name() in MyLines:
            print line.name()
            if (line.name() in 'StrippingB2XuMuNuBs2KSSLine' ) :
                line._prescale = 1.
            Lines_to_run += [line]
MyStream.appendLines( Lines_to_run)

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()
sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = 'StripTest' )
#            
################################################################################################################################                      
def createVeloTracks():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle = 'pion',  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])
    
createVeloTracks()        
        
def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    Alltracksprotos = ChargedProtoParticleMaker("ProtoPMaker")
    Alltracksprotos.Inputs = ["Rec/Track/Best"]
    Alltracksprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ Alltracksprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsAlltracksPions',  Particle = 'pion' ,  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo', 'Long' , 'Upstream'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ]) 
configIso()

LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
VeloPions      = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
AllPions       = MergedSelection( "AllPions", RequiredSelections =[ LongPions , UpStreamPions ]) #, VeloPions  ] )
AllPions_seq   = SelectionSequence('AllPions_seq', TopSelection = AllPions)
DaVinci().appendToMainSequence( [ AllPions_seq ])
#############################################################


#############################################################
# DecayTreeTuple 
tuple           = DecayTreeTuple("Bs2KmuNuTuple")
tuple.Inputs    = ['Phys/B2XuMuNuBs2KLine/Particles']
tuple.ToolList += [
    "TupleToolGeometry"
    ,"TupleToolKinematic"
    , "TupleToolPrimaries"
    , "TupleToolMCBackgroundInfo"
    # , "TupleToolTrackInfo"
    , "LoKi::Hybrid::TupleTool/LoKiTool"
    ]

tuple.ReFitPVs = True
tuple.OutputLevel = 6

tuple.Decay    = "[B_s0 -> ^K- ^mu+]CC"
tuple.Branches = {
    "muon_p"   : "[B_s0 ->  K- ^mu+]CC"
    ,"kaon_m"  : "[B_s0 -> ^K-  mu+]CC" 
    ,"Bs"      : "[B_s0 ->  K-  mu+]CC" 
     }

tuple.addTool(TupleToolDecay, name="Bs")
tuple.addTool(TupleToolDecay, name="kaon_m")
tuple.addTool(TupleToolDecay, name="muon_p")

TupleToolTrackInfo       = tuple.addTupleTool("TupleToolTrackInfo")
TupleToolTrackInfo.Verbose=True

TupleToolIsoGeneric              =  tuple.addTupleTool("TupleToolIsoGeneric")
TupleToolIsoGeneric.ParticlePath =  AllPions_seq.outputLocation() 
TupleToolIsoGeneric.VerboseMode = True
MCTruth_noniso          = TupleToolMCTruth('MCTruth_noniso')
MCTruth_noniso.ToolList = ["MCTupleToolHierarchy"]
LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
LoKiTool_noniso.Variables     = { "ETA" : "ETA" , "PHI" : "PHI" , "TRTYPE" : "TRTYPE"}
TupleToolIsoGeneric.ToolList += ["TupleToolMCTruth/MCTruth_noniso" , "LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
TupleToolIsoGeneric.addTool(LoKiTool_noniso)
TupleToolIsoGeneric.addTool(MCTruth_noniso)
TupleToolIsoGeneric.OutputLevel = 6

MCTupleToolHierarchyNTracks  =  tuple.addTupleTool("MCTupleToolHierarchyNTracks/MC_NTracks_Bs2KmuNuTuple")
MCTupleToolHierarchyNTracks.ParticlePath =  AllPions_seq.outputLocation()
# MCTupleToolHierarchyNTracks.FragmentationStudy = True
MCTupleToolHierarchyNTracks.Mother_id    = 531

# Add all availble Tracks infos 
UdrlyTr_Suffix = 'Tr_'
loki_var_udrlyTr = {
    UdrlyTr_Suffix + "PT"               : "PT",
    UdrlyTr_Suffix + "PX"               : "PX",
    UdrlyTr_Suffix + "PY"               : "PY",
    UdrlyTr_Suffix + "PZ"               : "PZ",
    UdrlyTr_Suffix + "P"                : "P",
    UdrlyTr_Suffix + "E"                : "E",
    UdrlyTr_Suffix + "Eta"              : "ETA",
    UdrlyTr_Suffix + "THETA"            : "atan ((PT/PZ))",
    UdrlyTr_Suffix + "Phi"              : "PHI",
    UdrlyTr_Suffix + "MinIP"            : "MIPDV(PRIMARY)",
    UdrlyTr_Suffix + "MinIPChi2"        : "MIPCHI2DV(PRIMARY)",
    UdrlyTr_Suffix + "BPVIP"            : "BPVIP()",
    UdrlyTr_Suffix + "BPVIPCHI2"        : "BPVIPCHI2()",
    UdrlyTr_Suffix + "PIDe"             : "PIDe",
    UdrlyTr_Suffix + "PIDmu"            : "PIDmu",
    UdrlyTr_Suffix + "PIDp"             : "PIDp",
    UdrlyTr_Suffix + "PIDK"             : "PIDK",
    UdrlyTr_Suffix + "PROBNNe"          : "PROBNNe"  ,  #"PPINFO(PROBNNe)"
    UdrlyTr_Suffix + "PROBNNmu"         : "PROBNNmu" ,  #"PPINFO(PROBNNmu)"
    UdrlyTr_Suffix + "PROBNNk"          : "PROBNNk"  ,  #"PPINFO(PROBNNk)"
    UdrlyTr_Suffix + "PROBNNpi"         : "PROBNNpi" ,  #"PPINFO(PROBNNpi)"
    UdrlyTr_Suffix + "PROBNNp"          : "PROBNNp"  ,  #"PPINFO(PROBNNp)"
    UdrlyTr_Suffix + "PROBNNghost"      : "PROBNNghost" ,#"PPINFO(PROBNNghost)"
    UdrlyTr_Suffix + "TRCHI2DOF"        : "TRCHI2DOF",
    UdrlyTr_Suffix + "TRPCHI2"          : "TRPCHI2",
    UdrlyTr_Suffix + "TRTYPE"           : "TRTYPE",    
    UdrlyTr_Suffix + "Charge"           : "Q",
    UdrlyTr_Suffix + "TRCLONEDIST"      : "CLONEDIST" ,
    UdrlyTr_Suffix + "TRGHOSTPROB"      : "TRGHOSTPROB",
    UdrlyTr_Suffix + "TrFIRSTHITZ"      : "TRFUN( TrFIRSTHITZ )" ,
    UdrlyTr_Suffix + "TrFITTCHI2NDOF"   : "TRFUN( TrFITTCHI2/TrFITTNDOF )" ,
    UdrlyTr_Suffix + "TRFITVELOCHI2NDOF": "TRFUN( TrFITVELOCHI2/TrFITVELONDOF )" ,
    UdrlyTr_Suffix + "TRFITTCHI2"       : "TRFUN( TrFITTCHI2 )" ,
    UdrlyTr_Suffix + "TRFITMATCHCHI2"   : "TRFUN( TrFITMATCHCHI2 )" ,
    UdrlyTr_Suffix + "HASMUON"          : "switch( HASMUON , 1., 0.)" ,
    UdrlyTr_Suffix + "ISMUON"           : "switch( ISMUON  , 1., 0.)" ,
    UdrlyTr_Suffix + "HASRICH"          : "switch( HASRICH , 1., 0.)" ,
    UdrlyTr_Suffix + "HcalE"            : "PPINFO( LHCb.ProtoParticle.CaloHcalE , -10)" ,
    UdrlyTr_Suffix + "EcalE"            : "PPINFO( LHCb.ProtoParticle.CaloEcalE , -10)" ,
    UdrlyTr_Suffix + "PrsE"             : "PPINFO( LHCb.ProtoParticle.CaloPrsE , -10)" ,
    UdrlyTr_Suffix + "NSHARED"          : "PPINFO( LHCb.ProtoParticle.MuonNShared , -1)",
    UdrlyTr_Suffix + "VeloCharge"       : "PPINFO( LHCb.ProtoParticle.VeloCharge , -10)"     
    }
loki_Avar_udrlyTr = {
    UdrlyTr_Suffix + "ADOCA" :  "ADOCA(1,2)",
    UdrlyTr_Suffix + "ACHI2DOCA":"ACHI2DOCA(1,2)",
    UdrlyTr_Suffix + "AALLSAMEBPV" : "switch(AALLSAMEBPV( -1, -1 , 0.8  ),0,1)"
    }

LoKiVariables_othertracks              =  tuple.addTupleTool("LoKi::Hybrid::ArrayTupleTool/LoKiVariables_othertracks")
LoKiVariables_othertracks.Preambulo    =  [ "from LoKiTracks.decorators import *" ]
LoKiVariables_othertracks.Variables    =  loki_var_udrlyTr
LoKiVariables_othertracks.AVariables   =  loki_Avar_udrlyTr
LoKiVariables_othertracks.Source       =  "SOURCE('" + AllPions_seq.outputLocation() + "', )"

LoKiVariables = tuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
LoKiVariables.Preambulo = [
     "from LoKiPhysMC.decorators import *",
     "from LoKiPhysMC.functions import mcMatch"
    ]

LoKiVariables.Variables = {
    "ETA"              : "ETA",
    "PHI"              : "PHI",
    "DOCA"             : "DOCA(1,2)",
    "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
    "FD_CHI2_LOKI"     : "BPVVDCHI2",
    "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
    "FD_S"             : "BPVDLS",
    "cosTheta1_star"   : "LV01",
    "cosTheta2_star"   : "LV02",
    #"COSPOL"          : "COSPOL",
    #"SINCHI"          : "SINCHI" ,
    #"COSCHI"          : "COSCHI" ,
    #"COSTHETATR"      : "COSTHETATR" ,
    #"SINPHITR"        : "SINPHITR" ,
    #"COSPHITR"        : "COSPHITR" ,
    #"MINANGLE"        : "MINANGLE()" ,
    "TruthMatched"     : "switch( mcMatch ('[ [B_s~0]cc ==> K+ mu- Neutrino ]CC', 1 )  , 1 , 0 )"
    }

LoKiVariables_K = tuple.kaon_m.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_K")
LoKiVariables_K.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
LoKiVariables_Mu = tuple.muon_p.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Mu")
LoKiVariables_Mu.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
############################################################################
TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.addTool(L0TriggerTisTos())
TupleToolTISTOS.addTool(TriggerTisTos())
TupleToolTISTOS.TriggerList=[
    "L0MuonDecision",
    "L0DiMuonDecision",        
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision"
    #"L0HadronDecision",
    #"Hlt2SingleMuonHighPTDecision",
    #"Hlt2Topo2BodySimpleDecision",
    #"Hlt2Topo2BodyBBDTDecision"
    ]

TupleToolTISTOS.VerboseL0   = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True
TupleToolTISTOS.TUS         = True
TupleToolTISTOS.TPS         = True

tuple.Bs.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.Bs.ToolList   += [ "TupleToolTISTOS"]

tuple.kaon_m.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.kaon_m.ToolList   += [ "TupleToolTISTOS"]

tuple.muon_p.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.muon_p.ToolList   += [ "TupleToolTISTOS"]

MCTruth = tuple.addTupleTool("TupleToolMCTruth")
#MCTruth.addTupleTool("MCTupleToolKinematic")
MCTruth.addTupleTool("MCTupleToolHierarchy")

MCTruth_k = tuple.kaon_m.addTupleTool("TupleToolMCTruth")
MCTruth_k.addTupleTool("MCTupleToolReconstructed")
MCTruth_k.addTupleTool("MCTupleToolHierarchy")
#MCTruth_k.addTupleTool("MCTupleToolKinematic")

MCTruth_mu = tuple.muon_p.addTupleTool("TupleToolMCTruth")
MCTruth_mu.addTupleTool("MCTupleToolReconstructed")
#MCTruth_mu.addTupleTool("MCTupleToolKinematic")
MCTruth_mu.addTupleTool("MCTupleToolHierarchy")

#MCTruth.muon_p.addTupleTool("MCTupleToolReconstructed")

from Configurables import TupleToolConeIsolation
tuple.kaon_m.addTupleTool("TupleToolConeIsolation")
tuple.kaon_m.TupleToolConeIsolation.FillAsymmetry     = True
tuple.kaon_m.TupleToolConeIsolation.FillDeltas        = True
tuple.kaon_m.TupleToolConeIsolation.FillComponents    = True
tuple.kaon_m.TupleToolConeIsolation.MinConeSize       = 0.5
tuple.kaon_m.TupleToolConeIsolation.SizeStep          = 0.5
tuple.kaon_m.TupleToolConeIsolation.MaxConeSize       = 2.0
tuple.kaon_m.TupleToolConeIsolation.FillPi0Info       = True
tuple.kaon_m.TupleToolConeIsolation.FillMergedPi0Info = True

tuple.muon_p.addTupleTool("TupleToolConeIsolation")
tuple.muon_p.TupleToolConeIsolation.FillAsymmetry     = True
tuple.muon_p.TupleToolConeIsolation.FillDeltas        = True
tuple.muon_p.TupleToolConeIsolation.FillComponents    = True
tuple.muon_p.TupleToolConeIsolation.MinConeSize       = 0.5
tuple.muon_p.TupleToolConeIsolation.SizeStep          = 0.5
tuple.muon_p.TupleToolConeIsolation.MaxConeSize       = 2.0

tuple.ToolList +=  [
    "TupleToolPropertime"
    , "TupleToolRecoStats"
    #, "TupleToolSLTools"
    , "TupleToolTrackPosition"
    #, "TupleToolRICHPid"
    #, "TupleToolMuonPid"
    , "TupleToolGeneration"
    ]

# Event Tuple
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
#etuple = tuple.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKiEvent")
# DaVinci settings
#

from Configurables import DaVinci
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().UserAlgorithms  += [ etuple , tuple ]
#DaVinci().UserAlgorithms += [ etuple , KmuNuSSTuple ]
DaVinci().MainOptions   = "" 
DaVinci().EvtMax        = 1000
DaVinci().PrintFreq     = 100000
DaVinci().DataType      = '2012'
# data 2012:
DaVinci().DDDBtag       =  "dddb-20120831" # MC12
DaVinci().CondDBtag     =  "sim-20121025-vc-mu100" # MC12
#from Configurables import CondDB
#CondDB().UseLatestTags = ["2012"]
# MC
DaVinci().Simulation = True
DaVinci().Lumi       = False        
#######################################################################
#
DaVinci().TupleFile = "DTT_Sig.root"

# Signal
importOptions("./Bs_KmuNu_MC.py")

