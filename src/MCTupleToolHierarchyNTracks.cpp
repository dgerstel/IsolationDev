// $Id: MCTupleToolHierarchyNTracks.cpp,v 1.0 2017-02-20 16:00:35 sakar $
// Include files
// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
// local
#include "MCTupleToolHierarchyNTracks.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>

#include "Event/Particle.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Kernel/IParticle2MCAssociator.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolHierarchyNTracks
//
// Author: AKAR SIMON
//-----------------------------------------------------------------------------
using namespace LHCb ;

//Declaration of Factory
DECLARE_TOOL_FACTORY( MCTupleToolHierarchyNTracks )
//=============================================================================
//============================================================================
MCTupleToolHierarchyNTracks::MCTupleToolHierarchyNTracks(const std::string& type,
							 const std::string& name,
							 const IInterface* parent )
: TupleToolBase ( type, name , parent ),
  m_descend(0)
{
  declareInterface<IParticleTupleTool>(this);
  
  declareProperty("ParticlePath", m_ParticlePath="/Event/Phys/StdAllNoPIDsPions/Particles" );
  //  declareProperty("FragmentationStudy", m_FragmentationStudy=true );
  declareProperty("Mother_id", m_signalB_id=531);
  
  m_p2mcAssocTypes.push_back( "DaVinciSmartAssociator" );
  m_p2mcAssocTypes.push_back( "MCMatchObjP2MCRelator"  );
  declareProperty( "IP2MCPAssociatorTypes", m_p2mcAssocTypes );
}
//=============================================================================
// =============================================================================
StatusCode MCTupleToolHierarchyNTracks::initialize()
{  
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
    
  m_descend = tool<IParticleDescendants> ( "ParticleDescendants", this );
  if( ! m_descend ) {fatal() << "Unable to retrieve ParticleDescendants tool "<< endmsg;
    return StatusCode::FAILURE; 
  }
  
  m_p2mcAssocs.clear();
  for ( std::vector<std::string>::const_iterator iMCAss = m_p2mcAssocTypes.begin();
  	iMCAss != m_p2mcAssocTypes.end(); ++iMCAss ) {
    m_p2mcAssocs.push_back( tool<IParticle2MCAssociator>(*iMCAss,this) );
  }
  if ( m_p2mcAssocs.empty() )
    {
      return Error("No MC associators configured");
    }
  
  return sc;
}

//=============================================================================
//=============================================================================
StatusCode MCTupleToolHierarchyNTracks::fill(const LHCb::Particle* mother, //mother
					     const LHCb::Particle* P, //P
					     const std::string& ,    //head
					     Tuples::Tuple& tuple)
{
  bool test = true;

  //------------------ Trivial checks ------------------
  // Check we have a mother 
  if (!mother) return StatusCode::SUCCESS;
  if (!(P->particleID().hasBottom())) return StatusCode::SUCCESS;

  //----------------------------------------------------
  //----------------------------------------------------
  // Store the signal B daughter (to remove them from the loop
  // over all the particles in the container)
  Particle::ConstVector sigCands = m_descend->descendants(mother);
  if ( sigCands.empty() ) return StatusCode::SUCCESS;

  // Store all the particles (to loop over)
  LHCb::Particle::Range allparts ;
  if (exist<LHCb::Particle::Range>(m_ParticlePath)) {
    allparts = get<LHCb::Particle::Range>(m_ParticlePath);
  } else {
    return Error("Nothing found at "+ m_ParticlePath , StatusCode::SUCCESS,1);
  }

  //----------------------------------------------------
  //----------------------------------------------------
  // Define the containers needed for the information to store 
  // For the signal B particle
  int mBs_id = 9;
  int mBs_key = -9;
  bool mBs_hasOsc = false;
  int mBs_anc_id = 9;
  int mBs_anc_key = -9;

  // For all the tracks (signal tracks removed)
  std::vector<int>  mcp_ids              ,  mcp_keys;
  std::vector<int>  mcp_anc_ids          ,  mcp_anc_keys;
  std::vector<int>  mcp_anc_dght_ids     ,  mcp_anc_dght_keys;
  std::vector<int>  mcp_mother_ids       ,  mcp_mother_keys;
  std::vector<int>  mcp_gd_mother_ids    ,  mcp_gd_mother_keys;
  std::vector<int>  mcp_gd_gd_mother_ids ,  mcp_gd_gd_mother_keys;
  
  std::vector<bool>    Tr_MC_OV_AssocToSignalPVs; 
  std::vector<bool>    mcp_OV_isPrimarys; 
  std::vector<int>     mcp_OV_productss;          
  std::vector<int>     mcp_OV_types ;          
  std::vector<double>  mcp_OV_Xs ;                
  std::vector<double>  mcp_OV_Ys ;                
  std::vector<double>  mcp_OV_Zs ;                 
   
  
  
  // Flags
  std::vector<int>  oriFlags, bFlags;
  

  //----------------------------------------------------
  //----------------------------------------------------
  // Get the MC information for the mother
  const LHCb::MCParticle* mBs(NULL);
  const LHCb::MCParticle* mBs_ancestor(NULL);
  const LHCb::MCVertex * BSignal_OV(NULL) ;
  // Retrieve MC particle associated to signal B
  for ( std::vector<IParticle2MCAssociator*>::const_iterator iMCAss = m_p2mcAssocs.begin();
	iMCAss != m_p2mcAssocs.end(); ++iMCAss ) {
    mBs = (*iMCAss)->relatedMCP(mother);
    if ( mBs ) {
      // Store the B info
      mBs_id  = mBs->particleID().pid();
      mBs_key = mBs->key();
      mBs_hasOsc = mBs->hasOscillated() ;
      // Get the ancestor of the signal B
      mBs_ancestor = getAncestor( mBs );
      if ( mBs_ancestor ) {
        // Store the B ancestor info
        mBs_anc_id  = mBs_ancestor->particleID().pid();
        mBs_anc_key = mBs_ancestor->key();
        if ( mBs_ancestor->originVertex() ) BSignal_OV = mBs_ancestor->originVertex();
      }
      break;
    }
  }
  
  //----------------------------------------------------
  //----------------------------------------------------
  // Make the loop over all the particles in the container
  LHCb::Particle::Range::const_iterator ite_P;
  for(ite_P = allparts.begin() ; ite_P != allparts.end() ; ++ite_P) {
    
  
    //-----------------------------------------
    //-----------------------------------------
    // Get the pointer to the particle
    const Particle * thisP  = (*ite_P);

    const LHCb::ProtoParticle* proto = thisP->proto();
    if(!proto) continue ;
    const LHCb::Track* track = proto->track();
    if(!track) continue ;

    //-----------------------------------------
    //-----------------------------------------
    // Check if this particle is in the list of signal particles
    // if go on only for other particles
    if ( isInDecay( sigCands, *ite_P ) ) continue ;
    
    
    //-----------------------------------------
    //-----------------------------------------
    // Local containers to the particles information
    int mcp_id = 9;
    int mcp_key = -9;
    int mcp_anc_id = 9;
    int mcp_anc_key = -9;
    int mcp_anc_dght_id = 9;
    int mcp_anc_dght_key = -9;
    
    int mcp_mom_id = 9;
    int mcp_mom_key = -9;
    int mcp_gd_mom_key = -9;
    int mcp_gd_mom_id = 9;
    int mcp_gd_gd_mom_key = -9;
    int mcp_gd_gd_mom_id = 9;
    
    bool Tr_MC_OV_AssocToSignalPV = false;
    bool mcp_OV_isPrimary  = false ;
    int mcp_OV_products    =-9; 
    int mcp_OV_type        =-9;
    double mcp_OV_X        =-9;    
    double mcp_OV_Y        =-9;    
    double mcp_OV_Z        =-9;    
    //-----------------------------------------
    //-----------------------------------------
    // Retrieve the MC information and store it
    const LHCb::MCParticle* mcp(NULL);
    const LHCb::MCParticle* mcp_ancestor(NULL);
    const LHCb::MCVertex *mcTrack_OV(NULL);
    const LHCb::MCParticle* mcp_ancestor_daught(NULL);
    const LHCb::MCParticle* mcpmom(NULL);
    const LHCb::MCParticle* mcpmom_mom(NULL);
    const MCParticle* mcpmom_mom_mom(NULL);
    for ( std::vector<IParticle2MCAssociator*>::const_iterator iMCAss = m_p2mcAssocs.begin();
  	  iMCAss != m_p2mcAssocs.end(); ++iMCAss ) {
      mcp = (*iMCAss)->relatedMCP(thisP);


      if ( mcp ) {
	//-----------------------------------------
	// Store the particle info
	mcp_id  = mcp->particleID().pid();
	mcp_key = mcp->key();
	// Store the particle mother info
	mcpmom = mcp->mother();

	if(mcpmom){
	  mcp_mom_id = mcpmom->particleID().pid();
	  mcp_mom_key = mcpmom->key();
	  // Store the particle gd mother info
	  mcpmom_mom = mcpmom->mother();

	  if(mcpmom_mom){
	    mcp_gd_mom_id = mcpmom_mom->particleID().pid();
	    mcp_gd_mom_key = mcpmom_mom->key();
	    // Store the particle gd gd mother info
	    mcpmom_mom_mom = mcpmom_mom->mother();

	    if(mcpmom_mom_mom){
	      mcp_gd_gd_mom_id = mcpmom_mom_mom->particleID().pid();
	      mcp_gd_gd_mom_key = mcpmom_mom_mom->key();

	    }
	  }
	}
	//-----------------------------------------
	// Get the particle ancestor
	mcp_ancestor = getAncestor( mcp );
  
  
	if ( mcp_ancestor ) {
	  // Store the particle ancestor info
	  mcp_anc_id  = mcp_ancestor->particleID().pid();
	  mcp_anc_key = mcp_ancestor->key();
    if ( mcp_ancestor->originVertex() )
    {
      mcTrack_OV               = mcp_ancestor->originVertex();
      if (mcTrack_OV == BSignal_OV && BSignal_OV!=NULL && mcTrack_OV !=NULL) Tr_MC_OV_AssocToSignalPV = true;
      mcp_OV_isPrimary = mcTrack_OV->isPrimary() ;
      //mcp_OV_isDecay   = mcTrack_OV->isDecay() ;
      mcp_OV_type      = mcTrack_OV->type();
      mcp_OV_products  = mcTrack_OV->products().size() ;
      mcp_OV_X         = mcTrack_OV->position().x() ;
      mcp_OV_Y         = mcTrack_OV->position().y() ;
      mcp_OV_Z         = mcTrack_OV->position().z() ;
    }
  }
  
  //-----------------------------------------
  // Get the particle ancestor daughter
  mcp_ancestor_daught = getAncestorDght( mcp );
  
  if ( mcp_ancestor_daught ) {
	  // Store the particle ancestor daughter info
	  mcp_anc_dght_id  = mcp_ancestor_daught->particleID().pid();
	  mcp_anc_dght_key = mcp_ancestor_daught->key();
	}
	break;
      }
    } // end of loop over the particle to mc associator

    //-----------------------------------------
    //-----------------------------------------
    // Now add the flags to ease the selection
    // of the fragmentation particles

    // Flag to tell if the particle is coming from the B
    // note that since we remove all the signal tracks,
    // this should never happen...
      int bFlag = 0;
      const LHCb::MCParticle* mcp_ancestor_b(NULL);
      if( mcp ) {
      mcp_ancestor_b = originof( mcp );
      
      
      if ( mcp_ancestor_b ) {
        
	if( mcp_ancestor_b->particleID().hasBottom() ) {
    
	  bFlag = 1;
	  // Check if ancestor is the B signal itself
	  if( (mcp_ancestor_b->particleID().pid() == mBs_id) && (mcp_ancestor_b->key() == mBs_key) )  bFlag = -1;
	  
	}
      }
    }
    
    //-----------------------------------------
    //-----------------------------------------
    // Flag the type of particle (fragmentation,
    // underlying event, ...)
    int oriFlag = -1;
    if ( mcp_ancestor ) {
      if ( mcp_ancestor->particleID().hasBottom() ) { 

	
	if ( (mcp_anc_key == mBs_anc_key) ) {
	  // This means the particle comes from
	  // the same b-quark than the signal B
	  oriFlag = 1;


	  if ( mcp_ancestor_daught ) {


	    if ( mcp_ancestor_daught->particleID().hasBottom() ) {
	      // Means that the particle and the signal B comes
	      // from an excited B meson
	      oriFlag = 2;


	      if ( abs(mcp_anc_dght_id) == 533 ||
		   abs(mcp_anc_dght_id) == 523 ||
		   abs(mcp_anc_dght_id) == 513 ) {
          

		oriFlag = 3;
	      }
	    }
	  }
	}
      }
      else {
	oriFlag = -abs(mcp_ancestor->particleID().pid()); 


      }
      if ( mcp_anc_key == mcp_key ) {
	// Means this particle has no mother, thus
	// is a prompt particle coming from soft QCD effects
	oriFlag = 0;
      }
    }

    
    //-----------------------------------------
    //-----------------------------------------
    //-----------------------------------------
    //-----------------------------------------
    // Store everything now
    // starting by the mc info
    mcp_ids.push_back(mcp_id);
    mcp_mother_ids.push_back(mcp_mom_id);
    mcp_gd_mother_ids.push_back(mcp_gd_mom_id);
    mcp_gd_gd_mother_ids.push_back(mcp_gd_gd_mom_id);

    mcp_keys.push_back(mcp_key);
    mcp_mother_keys.push_back(mcp_mom_key);
    mcp_gd_mother_keys.push_back(mcp_gd_mom_key);
    mcp_gd_gd_mother_keys.push_back(mcp_gd_gd_mom_key);

    mcp_anc_ids.push_back(mcp_anc_id);
    mcp_anc_keys.push_back(mcp_anc_key);
    
    mcp_anc_dght_ids.push_back(mcp_anc_dght_id);
    mcp_anc_dght_keys.push_back(mcp_anc_dght_key);
    
    // Store the flags value
    bFlags.push_back(bFlag);
    oriFlags.push_back(oriFlag);
    
    Tr_MC_OV_AssocToSignalPVs.push_back( Tr_MC_OV_AssocToSignalPV ); 
    mcp_OV_isPrimarys.push_back( mcp_OV_isPrimary);
    mcp_OV_productss.push_back(mcp_OV_products );
    mcp_OV_types.push_back( mcp_OV_type );
    mcp_OV_Xs.push_back( mcp_OV_X );        
    mcp_OV_Ys.push_back( mcp_OV_Y );        
    mcp_OV_Zs.push_back( mcp_OV_Z );        
    
    } // end of the loop over the particles
    
  
  //----------------------------------------------------
  //----------------------------------------------------
  // Fill the tuple now
  // // info related to the B signal
  test &= tuple -> column("Tr_MC_Bs_ID"             ,  mBs_id      );
  test &= tuple -> column("Tr_MC_Bs_Key"            ,  mBs_key     );
  test &= tuple -> column("Tr_MC_Bs_OSCIL"          ,  mBs_hasOsc  );
  test &= tuple -> column("Tr_MC_ANCESTOR_Bs_ID"    ,  mBs_anc_id  );
  test &= tuple -> column("Tr_MC_ANCESTOR_Bs_Key"   ,  mBs_anc_key );

  // info related to the mc particle ancestor
  test &= tuple -> farray("Tr_ANCESTOR_ID"          ,  mcp_anc_ids            ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_ANCESTOR_Key"         ,  mcp_anc_keys           ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_ANCESTOR_DAUGHTER_ID" ,  mcp_anc_dght_ids       ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_ANCESTOR_DAUGHTER_Key",  mcp_anc_dght_keys      ,  "N_MC" , 500);
  // info related to the flags
  test &= tuple -> farray("Tr_ORIG_FLAGS"           ,  oriFlags               ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_B_FLAGS"              ,  bFlags                 ,  "N_MC" , 500);
    
  // info related to the mc particle (mother, gd mom, gd gd mom)
  test &= tuple -> farray("Tr_MC_ID"                ,  mcp_ids                ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_Key"               ,  mcp_keys               ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_MOTHER_ID"         ,  mcp_mother_ids         ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_MOTHER_Key"        ,  mcp_mother_keys        ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_GD_MOTHER_ID"      ,  mcp_gd_mother_ids      ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_GD_MOTHER_Key"     ,  mcp_gd_mother_keys     ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_GD_GD_MOTHER_ID"   ,  mcp_gd_gd_mother_ids   ,  "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_GD_GD_MOTHER_Key"  ,  mcp_gd_gd_mother_keys  ,  "N_MC" , 500);

  test &= tuple -> farray("Tr_MC_OV_AssocToSignalPV" , Tr_MC_OV_AssocToSignalPVs,  "N_MC" , 500); 
  test &= tuple -> farray("Tr_MC_OV_isPrimary" , mcp_OV_isPrimarys, "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_OV_types"     , mcp_OV_types     , "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_OV_products"  , mcp_OV_productss , "N_MC" , 500);
  test &= tuple -> farray("Tr_MC_OV_X"         ,      mcp_OV_Xs   , "N_MC" , 500);                
  test &= tuple -> farray("Tr_MC_OV_Y"         ,      mcp_OV_Ys   , "N_MC" , 500);                
  test &= tuple -> farray("Tr_MC_OV_Z"         ,      mcp_OV_Zs   , "N_MC" , 500);                
  return StatusCode(test);
  
  }
  
  
//==================================================================================
//==================================================================================
bool MCTupleToolHierarchyNTracks::isInDecay( const LHCb::Particle::ConstVector daugs,
					     const LHCb::Particle* particle ) const
{
  bool isInDecay = false;
  const LHCb::Track* p_tr = particle->proto()->track();
  Particle::ConstVector::const_iterator it_part;
  for( it_part= daugs.begin(); it_part!= daugs.end(); it_part++)
  {
    if(!(*it_part)->isBasicParticle()) continue;
    const LHCb::Track* Track_decay = (*it_part)->proto()->track();
    if(Track_decay && p_tr){
      if(Track_decay == p_tr){
        isInDecay = true;
      } 
    }
  }
  return isInDecay;
}

//==================================================================================
//==================================================================================
const MCParticle* MCTupleToolHierarchyNTracks::originof( const LHCb::MCParticle* mcp )
{
  const MCParticle* mcp_mom = mcp->mother();
  if ( (!mcp_mom) || mcp->particleID().hasBottom() ) return mcp; 
  else return originof( mcp_mom );
}

//==================================================================================
const MCParticle* MCTupleToolHierarchyNTracks::getAncestor( const LHCb::MCParticle* mcp )
{
  MCParticle *ancestor  = const_cast<LHCb::MCParticle*>( mcp );
  while( ancestor->mother() ) {
    ancestor = const_cast<LHCb::MCParticle*>(ancestor->mother());
  }
  return ancestor;
}

//==================================================================================
//==================================================================================
const MCParticle* MCTupleToolHierarchyNTracks::getAncestorDght( const LHCb::MCParticle* mcp )
{
  MCParticle *ancestor      = const_cast<LHCb::MCParticle*>( mcp );
  MCParticle *ancestorDght  = const_cast<LHCb::MCParticle*>( mcp );
  while( ancestor->mother() ) {
    ancestorDght = ancestor;    
    ancestor = const_cast<LHCb::MCParticle*>(ancestor->mother());
  }
  return ancestorDght;
}
