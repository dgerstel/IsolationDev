// $Id: MCTupleToolHierarchyNTracks.h,v 1.0 2017-02-20 16:00:35 sakar $
#ifndef MCTUPLETOOLHIERARCHYNTRACKS_H 
#define MCTUPLETOOLHIERARCHYNTRACKS_H 1

// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"                   // Interface
#include "Kernel/IParticleDescendants.h"

#include <Kernel/IDVAlgorithm.h>
#include <Kernel/GetIDVAlgorithm.h>

// struct Particle2MCLinker;
#include "Kernel/Particle2MCLinker.h"
#include "Kernel/IDaVinciAssociatorsWrapper.h"

class IParticle2MCAssociator;

class MCTupleToolHierarchyNTracks : public TupleToolBase,
                                    virtual public IParticleTupleTool
{

 public:
  
  /// Standard constructor
  MCTupleToolHierarchyNTracks( const std::string& type, 
                       const std::string& name,
                       const IInterface* parent);
  
  virtual ~MCTupleToolHierarchyNTracks( ){}; ///< Destructor
  
  virtual StatusCode initialize();
  
  virtual StatusCode fill( const LHCb::Particle*,
			   const LHCb::Particle*,
			   const std::string&,
			   Tuples::Tuple& );

 private:
  
  IParticleDescendants* m_descend;

  /* bool m_FragmentationStudy; */
  int m_signalB_id;
  std::string m_ParticlePath;
  std::vector<IParticle2MCAssociator*> m_p2mcAssocs;
  std::vector<std::string> m_p2mcAssocTypes;

  bool isInDecay( const LHCb::Particle::ConstVector daugs , 
                  const LHCb::Particle* particle ) const ;

  const LHCb::MCParticle* originof( const LHCb::MCParticle* );

  const LHCb::MCParticle* getAncestor( const LHCb::MCParticle* );

  const LHCb::MCParticle* getAncestorDght( const LHCb::MCParticle* );

  int comes_from_excitedB(const LHCb::MCParticle* BS,
			  const LHCb::MCParticle* mcp,
			  int flag );

};
//============================================================//
#endif // MCTUPLETOOLHIERARCHYNTRACKS_H
