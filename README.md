- Codes to make the training tuples for the isolation algorithm are in `src/`
- Example scripts to run the codes are in `scripts/`

# Setup
`setup.sh` builds DV dev locally, copies and compiles the isolation tuple tools there.

# Running
`<path/to/your/DV/dev>/run gaudirun.py <your_Decay_Option_Script.py>`

The scripts in `./scripts/` assume you `cd ./scripts/` before running the above.
